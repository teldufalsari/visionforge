# Module visionforge-jupyter-gdml

Jupyter api artifact for GDML rendering

## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:visionforge-jupyter-gdml:0.2.0`.

**Gradle Groovy:**
```groovy
repositories {
    maven { url 'https://repo.kotlin.link' }
    mavenCentral()
}

dependencies {
    implementation 'space.kscience:visionforge-jupyter-gdml:0.2.0'
}
```
**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:visionforge-jupyter-gdml:0.2.0")
}
```
